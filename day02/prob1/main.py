#!/usr/bin/python3
from functools import reduce
from copy import deepcopy

def averager(data, i):
    avg = sum([line[i] for line in data])/len(data)
    if avg == .5:
        return 2
    else:
        return int(avg > .5)

with open('input.txt', 'r') as f:
    data = [[int(bit) for bit in list(line)] for line in f.read().split()]

oxi = deepcopy(data)
carb = deepcopy(data)
# Oxi
i = 0
while len(oxi) > 1:
    averages = averager(oxi, i)
    if averages == 2:
        oxi = list(filter(lambda x: x[i] == 1, oxi))
    else:
        oxi = list(filter(lambda x: x[i] == averages, oxi))
    i += 1

# Carb
i = 0
while len(carb) > 1:
    averages = averager(carb, i)
    if averages == 2:
        carb = list(filter(lambda x: x[i] == 0, carb))
    else:
        carb = list(filter(lambda x: x[i] != averages, carb))
    i += 1

oxi = reduce(lambda x, y: (x << 1) + int(y), oxi[0])
carb = reduce(lambda x, y: (x << 1) + int(y), carb[0])

print (f"lifesupport: {oxi * carb}, O2: {oxi}, CO2: {carb}")
