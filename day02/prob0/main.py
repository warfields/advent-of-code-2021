#!/usr/bin/python3
from functools import reduce
with open('input.txt', 'r') as f:
    data = [[int(bit) for bit in list(line)] for line in f.read().split()]
 
averages = [sum([line[col] for line in data])/len(data) for col in range(len(data[0]))]

gamma = reduce(lambda x, y: (x << 1) + int(y) ,(map(lambda x: x > .5, averages)))
epsilon = reduce(lambda x, y: (x << 1) + int(y) ,(map(lambda x: x < .5, averages)))

print (f"Gamma: {gamma}   epsilon: {epsilon}    power: {gamma * epsilon}")