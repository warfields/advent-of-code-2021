#!/usr/bin/python3

data = []

with open('input.txt', 'r') as f:
    for line in f:
        data.append(line[:-1])

largest = 0

proc = []

for addr in data:
    hob = addr[:-3]
    hob = hob.replace("B","1")
    hob = hob.replace("F","0")
    hob = int(hob,2)

    lob = addr[-3:]
    lob = lob.replace("R","1")
    lob = lob.replace("L","0")
    lob = int(lob, 2)

    addr = hob * 8 + lob
    proc.append(addr)
    if addr > largest:
        largest = addr

proc.sort()
prev = proc[0]
for x in proc[1:]:
    if x-1 != prev:
        print(x-1)
        break
    prev = x

