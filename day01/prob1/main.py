#!/usr/bin/python3
from functools import reduce
dirs = {'forward': (1,0), 'down':(0,1), 'up':(0,-1)}


with open('input.txt', 'r') as f:
    lines = [list(map(lambda x: x * int(pair[1]), dirs[pair[0]])) for pair in [line.split(" ") for line in f.read().split("\n")]]

depth = reduce(lambda cur, next: [cur[0]+ (cur[1] + next[1]) * next[0], cur[1] + next[1]], lines, [0,0])[0]
print (reduce(lambda x, y: x * y, [sum([line[0] for line in lines]), depth]))
