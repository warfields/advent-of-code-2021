#!/usr/bin/python3
with open('input.txt', 'r') as f:
    nums = [int(x) for x in f.read().split()]

inc = 0
prev = nums[0]

for x in nums[1::]:
    if x > prev:
        inc += 1
    prev = x

print(inc)