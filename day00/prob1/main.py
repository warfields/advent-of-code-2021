#!/usr/bin/python3
with open('input.txt', 'r') as f:
    nums = [int(x) for x in f.read().split()]

inc = 0
prev = sum(nums[0:2])

for x in range(1,len(nums)):
    window = sum(nums[x:x+3])
    print(f'{prev}:{window} x={x} nums=[{nums[x:x+2]}')
    if window > prev:
        inc += 1
    prev = window

print(inc)